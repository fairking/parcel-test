﻿using CsvHelper;
using CsvHelper.Configuration;
using ParcelTest.Entities;
using ParcelTest.Types;
using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ParcelTest.Helpers
{
    public static class CsvParserHelper
    {
        const string Col_OrderNo = "Order No";
        const string Col_ConsignmentNo = "Consignment No";
        const string Col_ParcelCode = "Parcel Code";
        const string Col_ConsigneeName = "Consignee Name";
        const string Col_AddressLine1 = "Address 1";
        const string Col_AddressLine2 = "Address 2";
        const string Col_City = "City";
        const string Col_State = "State";
        const string Col_CountryCode = "Country Code";
        const string Col_ItemQuantity = "Item Quantity";
        const string Col_ItemValue = "Item Value";
        const string Col_ItemWeight = "Item Weight";
        const string Col_ItemDescription = "Item Description";
        const string Col_ItemCurrency = "Item Currency";

        public static OrderBatch Parse(string fullCsvPath)
        {
            var batch = new OrderBatch();

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                TrimOptions = TrimOptions.Trim,
            };

            using (var reader = new StreamReader(fullCsvPath))
            using (var csv = new CsvReader(reader, config))
            {
                csv.Read();

                csv.ReadHeader();

                while (csv.Read())
                {
                    // Get/Create Order
                    var orderNo = csv.GetField<string>(Col_OrderNo);
                    var order = batch.Orders.SingleOrDefault(x => x.OrderNo.Equals(orderNo));
                    if (order == null)
                    {
                        order = new Order(orderNo);
                        batch.AddOrder(order);
                    }

                    // Get/Create Consignment
                    var consignmentNo = csv.GetField<string>(Col_ConsignmentNo);
                    var consignment = order.Consignments.SingleOrDefault(x => x.ConsignmentNo.Equals(consignmentNo));
                    if (consignment == null)
                    {
                        consignment = new Consignment(
                            consignmentNo,
                            csv.GetField<string>(Col_ConsigneeName),
                            csv.GetField<string>(Col_AddressLine1),
                            csv.GetField<string>(Col_AddressLine2),
                            csv.GetField<string>(Col_City),
                            csv.GetField<string>(Col_State),
                            csv.GetField<string>(Col_CountryCode)
                        );
                        order.AddConsignment(consignment);
                    }

                    // Get/Create Parcel
                    var parcelCode = csv.GetField<string>(Col_ParcelCode);
                    var parcel = consignment.Parcels.SingleOrDefault(x => x.ParcelCode.Equals(parcelCode));
                    if (parcel == null)
                    {
                        parcel = new Parcel(parcelCode);
                        consignment.AddParcel(parcel);
                    }

                    // Create ParcelItem
                    var currency = csv.GetField<string>(Col_ItemCurrency);
                    var parcelItem = new ParcelItem(
                        csv.GetField<string>(Col_ItemDescription),
                        csv.GetField<int>(Col_ItemQuantity),
                        csv.GetField<decimal>(Col_ItemWeight),
                        new Money(
                            csv.GetField<decimal>(Col_ItemValue),
                            !string.IsNullOrWhiteSpace(currency) ? Enum.Parse<Money.CurrencyEnum>(currency) : Money.CurrencyEnum.GBP
                        )
                    );
                    parcel.AddItem(parcelItem);
                }
            }

            batch.RecalculateTotals();

            return batch;
        }
    }
}
