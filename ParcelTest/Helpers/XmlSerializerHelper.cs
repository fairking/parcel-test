﻿using System.IO;
using System.Runtime.Serialization;

namespace ParcelTest.Helpers
{
    public static class XmlSerializerHelper
    {
        public static void Serialize<T>(this T obj, string destPath)
        {
            var serializer = new DataContractSerializer(typeof(T));

            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                serializer.WriteObject(fileStream, obj);
            }
        }

        public static T Deserialize<T>(string srcPath)
        {
            var serializer = new DataContractSerializer(typeof(T));
            using (var reader = new FileStream(srcPath, FileMode.Open))
            {
                return (T)serializer.ReadObject(reader);
            }
        }
    }
}
