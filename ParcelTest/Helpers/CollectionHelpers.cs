﻿using System;
using System.Collections.Generic;

namespace ParcelTest.Helpers
{
    public static class CollectionHelpers
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> method)
        {
            foreach (var item in items)
                method.Invoke(item);
        }

        /// <summary>
        /// ForEach with Index
        /// </summary>
        public static void ForEach<T>(this IEnumerable<T> items, Action<T, int> method)
        {
            int idx = 0;
            foreach (var item in items)
                method.Invoke(item, idx++);
        }

    }
}
