﻿using System;
using System.Runtime.Serialization;

namespace ParcelTest.Types
{
    [DataContract]
    public struct Money : IEquatable<Money>
    {
        [DataMember(Name = nameof(Value))]
        private decimal _value;

        [DataMember(Name = nameof(Currency))]
        private CurrencyEnum _currency;

        public Money(decimal value, CurrencyEnum currency)
        {
            _value = value;
            _currency = currency;
        }

        #region Properties

        public decimal Value => _value;

        public CurrencyEnum Currency => _currency;

        #endregion Properties

        #region Overloadings

        public static bool operator ==(Money left, Money right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Money left, Money right)
        {
            return !(left == right);
        }

        public static Money operator +(Money a) => a;
        public static Money operator -(Money a) => new Money(-a.Value, a.Currency);

        public static Money operator +(Money a, Money b)
        {
            if (a.Currency != b.Currency)
                throw new ArgumentException("Arithmetic operations are not supported between different currencies.");

            return new Money(a.Value + b.Value, a.Currency);
        }

        public static Money operator -(Money a, Money b)
        {
            if (a.Currency != b.Currency)
                throw new ArgumentException("Arithmetic operations are not supported between different currencies.");

            return new Money(a.Value - b.Value, a.Currency);
        }

        public static Money operator *(Money a, decimal b)
        {
            return new Money(a.Value * b, a.Currency);
        }

        public static Money operator /(Money a, decimal b)
        {
            if (b == 0)
                throw new DivideByZeroException();

            return new Money(a.Value / b, a.Currency);
        }

        #endregion Overloadings

        #region Overrides

        public override bool Equals(object obj)
        {
            return obj is Money money && Equals(money);
        }

        public bool Equals(Money other)
        {
            return Value == other.Value &&
                   Currency == other.Currency;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, Currency);
        }

        public override string ToString()
        {
            return $"{Value} {Currency}";
        }

        #endregion Overrides

        #region Internal Types

        /// <summary>
        /// Supported currencies
        /// </summary>
        public enum CurrencyEnum
        {
            /// <summary>
            /// Pound Sterling
            /// </summary>
            GBP,

            /// <summary>
            /// Euro
            /// </summary>
            EUR,

            /// <summary>
            /// US Dollar
            /// </summary>
            USD,
        }

        #endregion Internal Types
    }
}
