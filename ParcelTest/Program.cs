﻿using ParcelTest.Helpers;
using System;
using System.IO;

namespace ParcelTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var csvPath = Path.Combine(AppContext.BaseDirectory, "Files");
            foreach (var csvFile in Directory.GetFiles(csvPath, "*.csv"))
            {
                try
                {
                    var csvFileName = Path.GetFileNameWithoutExtension(csvFile);
                    Console.Write($"Process file '{csvFileName}'...");

                    var batch = CsvParserHelper.Parse(csvFile);

                    var xmlFile = Path.Combine(csvPath, csvFileName + ".xml");
                    XmlSerializerHelper.Serialize(batch, xmlFile);

                    File.Delete(csvFile);

                    Console.WriteLine($"OK");
                }
                catch (Exception exc)
                {
                    // TODO: Log error here..

                    Console.WriteLine($"ERROR");
                }
            }

            Console.WriteLine("Press Enter to exit...");
            Console.ReadLine();
        }
    }
}
