﻿using ParcelTest.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ParcelTest.Entities
{
    [DataContract]
    [KnownType(typeof(HashSet<Parcel>))]
    public class Consignment : IEntity, IHaveAddress
    {
        [DataMember(Name = nameof(Parcels))]
        private readonly ISet<Parcel> _parcels;

        protected Consignment()
        {
            _parcels = new HashSet<Parcel>();
        }

        public Consignment(
            string consignmentNo, string consigneeName, 
            string addressLine1, string addressLine2, string city, string state, string countryCode
            ) : this()
        {
            if (string.IsNullOrWhiteSpace(consignmentNo))
                throw new ArgumentNullException(nameof(consignmentNo));
            else
                ConsignmentNo = consignmentNo;

            if (string.IsNullOrWhiteSpace(consigneeName))
                throw new ArgumentNullException(nameof(consigneeName));
            else
                ConsigneeName = consigneeName;

            if (string.IsNullOrWhiteSpace(addressLine1))
                throw new ArgumentNullException(nameof(addressLine1));
            else
                AddressLine1 = addressLine1;

            AddressLine2 = addressLine2;

            City = city;

            State = state;

            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentNullException(nameof(countryCode));
            else
                CountryCode = countryCode;
        }

        #region Properties

        [DataMember]
        public string ConsignmentNo { get; protected set; }

        [DataMember]
        public string ConsigneeName { get; protected set; }

        [DataMember]
        public string AddressLine1 { get; protected set; }

        [DataMember]
        public string AddressLine2 { get; protected set; }

        [DataMember]
        public string City { get; protected set; }

        [DataMember]
        public string State { get; protected set; }

        [DataMember]
        public string CountryCode { get; protected set; }

        public IEnumerable<Parcel> Parcels => _parcels;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new <see cref="Parcel"/> to the <see cref="Parcels" /> collection.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if the argument <paramref name="parcel"/> is null.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="parcel"/> already exists in <see cref="Parcels" />.</exception>
        public void AddParcel(Parcel parcel)
        {
            if (parcel == null)
                throw new ArgumentNullException(nameof(parcel));

            if (_parcels.Contains(parcel))
                throw new ArgumentException($"Parcel '{parcel}' already exists.");

            _parcels.Add(parcel);
        }

        #endregion Methods

        #region Overrides

        public override bool Equals(object obj)
        {
            return obj is Consignment consignment &&
                   ConsignmentNo == consignment.ConsignmentNo;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ConsignmentNo);
        }

        public override string ToString()
        {
            return $"[{ConsignmentNo}] {ConsigneeName}";
        }

        #endregion Overrides
    }
}
