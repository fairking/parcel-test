﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ParcelTest.Entities
{
    [DataContract]
    [KnownType(typeof(HashSet<Consignment>))]
    public class Order
    {
        [DataMember(Name = nameof(Consignments))]
        private readonly ISet<Consignment> _consignments;

        protected Order()
        {
            _consignments = new HashSet<Consignment>();
        }

        public Order(string orderNo) : this()
        {
            if (string.IsNullOrWhiteSpace(orderNo))
                throw new ArgumentNullException(nameof(orderNo));

            OrderNo = orderNo;
        }

        #region Properties

        [DataMember]
        public string OrderNo { get; protected set; }

        public IEnumerable<Consignment> Consignments => _consignments;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new Consignment to the <see cref="Consignments" /> collection.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if the argument <paramref name="consignment"/> is null.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="consignment"/> already exists in <see cref="Consignments" />.</exception>
        public void AddConsignment(Consignment consignment)
        {
            if (consignment == null)
                throw new ArgumentNullException(nameof(consignment));

            if (_consignments.Contains(consignment))
                throw new ArgumentException($"Consignment '{consignment}' already exists.");

            _consignments.Add(consignment);
        }

        #endregion Methods

        #region Overrides

        public override bool Equals(object obj)
        {
            return obj is Order order &&
                   OrderNo == order.OrderNo;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(OrderNo);
        }

        public override string ToString()
        {
            return OrderNo;
        }

        #endregion Overrides
    }
}
