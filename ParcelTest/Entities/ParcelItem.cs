﻿using ParcelTest.Entities.Abstracts;
using ParcelTest.Types;
using System;
using System.Runtime.Serialization;

namespace ParcelTest.Entities
{
    [DataContract]
    public class ParcelItem : IEntity
    {
        protected ParcelItem()
        {

        }

        public ParcelItem(string description, int quantity, decimal weight, Money value) : this()
        {
            if (string.IsNullOrWhiteSpace(description))
                throw new ArgumentNullException(nameof(description));
            else
                Description = description;

            if (quantity <= 0)
                throw new ArgumentException("Quantity must be greater than 0", nameof(quantity));
            else
                Quantity = quantity;

            if (weight <= 0)
                throw new ArgumentException("Weight must be greater than 0", nameof(weight));
            else
                Weight = weight;

            if (value.Value < 0)
                throw new ArgumentException("Value must be greater than 0", nameof(value));
            else
                Value = value;
        }

        #region Properties

        [DataMember]
        public int Quantity { get; protected set; }

        /// <summary>
        /// Value of the item
        /// </summary>
        [DataMember]
        public Money Value { get; protected set; }

        /// <summary>
        /// Weight of the item measured in kg
        /// </summary>
        [DataMember]
        public decimal Weight { get; protected set; }

        [DataMember]
        public string Description { get; protected set; }

        #endregion Properties

        #region Overrides

        public override string ToString()
        {
            return $"{Description} - {Quantity} items";
        }

        public override bool Equals(object obj)
        {
            return obj is ParcelItem item &&
                   Quantity == item.Quantity &&
                   Value.Equals(item.Value) &&
                   Weight == item.Weight &&
                   Description == item.Description;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Quantity, Value, Weight, Description);
        }

        #endregion Overrides
    }
}
