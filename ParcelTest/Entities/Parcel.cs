﻿using ParcelTest.Entities.Abstracts;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ParcelTest.Entities
{
    [DataContract]
    [KnownType(typeof(HashSet<ParcelItem>))]
    public class Parcel : IEntity
    {
        [DataMember(Name = nameof(Items))]
        private readonly ISet<ParcelItem> _items;

        protected Parcel()
        {
            _items = new HashSet<ParcelItem>();
        }

        public Parcel(string parcelCode) : this()
        {
            if (string.IsNullOrWhiteSpace(parcelCode))
                throw new ArgumentNullException(nameof(parcelCode));

            ParcelCode = parcelCode;
        }


        #region Properties

        [DataMember]
        public string ParcelCode { get; protected set; }

        public IEnumerable<ParcelItem> Items => _items;

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new <see cref="ParcelItem"/> to the <see cref="Items" /> collection.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if the argument <paramref name="item"/> is null.</exception>
        public void AddItem(ParcelItem item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _items.Add(item);
        }

        #endregion Methods

        #region Events

        public event EventHandler<EventArgs> TotalsChangedEvent;

        #endregion Events

        #region Overrides

        public override bool Equals(object obj)
        {
            return obj is Parcel parcel &&
                   ParcelCode == parcel.ParcelCode;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ParcelCode);
        }

        public override string ToString()
        {
            return ParcelCode;
        }

        #endregion Overrides
    }
}
