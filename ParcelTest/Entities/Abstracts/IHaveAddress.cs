﻿namespace ParcelTest.Entities.Abstracts
{
    public interface IHaveAddress
    {
        string AddressLine1 { get; }

        string AddressLine2 { get; }

        string City { get; }

        string State { get; }

        string CountryCode { get; }
    }
}
