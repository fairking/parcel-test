﻿using ParcelTest.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace ParcelTest.Entities
{
    [DataContract]
    [KnownType(typeof(HashSet<Order>))]
    public class OrderBatch
    {
        [DataMember(Name = nameof(Orders))]
        private readonly ISet<Order> _orders;

        public OrderBatch()
        {
            _orders = new HashSet<Order>();
        }

        #region Properties

        public IEnumerable<Order> Orders => _orders;

        [DataMember]
        public IEnumerable<Money> TotalValue { get; protected set; }

        [DataMember]
        public decimal? TotalWeight { get; protected set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Adds a new <see cref="Order"/> to the <see cref="Orders" /> collection.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if the argument <paramref name="order"/> is null.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="order"/> already exists in <see cref="Orders" />.</exception>
        public void AddOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            if (_orders.Contains(order))
                throw new ArgumentException($"Consignment '{order}' already exists.");

            _orders.Add(order);
        }

        public void RecalculateTotals()
        {
            // Get all ParcelItems
            var items = Orders
                .SelectMany(o => o.Consignments.SelectMany(c => c.Parcels.SelectMany(p => p.Items)));

            // Calculate TotalValue by grouping all currencies together
            TotalValue = new HashSet<Money>(
                items
                    .Select(x => x.Value)
                    .GroupBy(x => x.Currency)
                    .Select(x => new Money(x.Sum(v => v.Value), x.Key))
            );

            // Calculate TotalWeight
            TotalWeight = items.Sum(x => x.Weight);
        }

        #endregion Methods

        #region Overrides

        public override bool Equals(object obj)
        {
            return obj is OrderBatch batch &&
                _orders.OrderBy(x => x.GetHashCode())
                    .SequenceEqual(batch._orders.OrderBy(x => x.GetHashCode()));
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_orders);
        }

        #endregion Overrides
    }
}
