# Introduction

## Instructions

The purpose of the exercise is to evaluate your approach to software development including object-oriented design, design patterns and testing.
- **Complete** the exercise in C#
- **Structure** your code as if this were a real production application
- **State** any assumptions you make as comments in the code. If any aspect of the specification is unclear, state your interpretation of the requirement in a comment.

## Task

Write a **c# console application** that monitors a directory for csv files, reads them in and outputs a file in XML format.

The **CSV** to be imported has 14 columns and a header (example attached: `CandidateTest_ManifestExample.csv`). The 14th column is optional. If it is blank then the value should default to **GBP**.

Each line in the CSV file (aside from the header) represents an **item in a parcel**. An item belongs to a parcel if it matches the **Parcel Code**. Parcels belong to a **Consignment** if the Consignment No matches. Consignments belong to an **Order** if the Order No matches.

The **XML** file output should have the following nodes:
- Orders
- Order
- Consignments
- Consignment
- Parcels
- Parcel
- Parcel Items
- Parcel Item

Additionally **within** the Orders node there should be a **Total Value** and **Total Weight** node representing the total value and total weight of all the consignments for that Order. 

## Deliverables

A **.Net** project written in C# that meets the requirements of the task. Please include all files required to **build** and **run** the software.


# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [.NET Hello World tutorial](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/)

If you're new to .NET you'll want to check out the tutorial, but if you're
already a seasoned developer considering building your own .NET app with GitLab,
this should all look very familiar.

## What's contained in this project

The root of the repository contains the out of the `dotnet new console` command,
which generates a new console application. Such application generates all found `*.csv` files
in `Files` folder into the XML format based on the above recomendations. Please create an issue
if you find any mistakes or errors [here](https://gitlab.com/fairking/parcel-test/-/issues/new).

In addition to the .NET Core content, there is a ready-to-go `.gitignore` file
sourced from the the .NET Core [.gitignore](https://github.com/dotnet/core/blob/master/.gitignore). This
will help keep your repository clean of build files and other configuration.

Finally, the `.gitlab-ci.yml` contains the configuration needed for GitLab
to build your code. Let's take a look, section by section.

First, we note that we want to use the official Microsoft .NET SDK image
to build our project.

```
image: microsoft/dotnet:latest
```

We're defining two stages here: `build`, and `test`. As your project grows
in complexity you can add more of these.

```
stages:
    - build
    - test
```

Next, we define our build job which simply runs the `dotnet build` command and
identifies the `bin` folder as the output directory. Anything in the `bin` folder
will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

Similar to the build step, we get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```

This should be enough to get you started. There are many, many powerful options 
for your `.gitlab-ci.yml`. You can read about them in our documentation 
[here](https://docs.gitlab.com/ee/ci/yaml/).

## Developing with Gitpod

This template repository also has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

The `.gitpod.yml` ensures that, when you open this repository in Gitpod, you'll get a cloud workspace with .NET Core pre-installed, and your project will automatically be built and start running.

