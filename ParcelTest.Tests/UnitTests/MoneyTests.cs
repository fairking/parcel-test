using ParcelTest.Types;
using System;
using Xunit;

namespace ParcelTest.Tests.UnitTests
{
    public class MoneyTests
    {
        [Fact]
        public void can_print_value()
        {
            var result = new Money(12.34m, Money.CurrencyEnum.GBP);
            Assert.Equal("12.34 GBP", $"{result}");
        }

        [Fact]
        public void can_perform_arithmetic_operations()
        {
            // Addition
            {
                var result = new Money(10.2m, Money.CurrencyEnum.GBP) + new Money(2.25m, Money.CurrencyEnum.GBP);
                Assert.Equal(12.45m, result.Value);
            }

            // Addition with different currencies
            {
                Func<object> result = () => new Money(10.2m, Money.CurrencyEnum.GBP) + new Money(2.25m, Money.CurrencyEnum.EUR);
                Assert.Throws<ArgumentException>(result);
            }

            // Subtraction
            {
                var result = new Money(10.2m, Money.CurrencyEnum.GBP) - new Money(2.25m, Money.CurrencyEnum.GBP);
                Assert.Equal(7.95m, result.Value);
            }

            // Subtraction with different currencies
            {
                Func<object> result = () => new Money(10.2m, Money.CurrencyEnum.GBP) - new Money(2.25m, Money.CurrencyEnum.EUR);
                Assert.Throws<ArgumentException>(result);
            }

            // Multiplication
            {
                var result = new Money(10.22m, Money.CurrencyEnum.GBP) * 2;
                Assert.Equal(20.44m, result.Value);
            }

            // Division
            {
                var result = new Money(10.2m, Money.CurrencyEnum.GBP) / 2;
                Assert.Equal(5.1m, result.Value);
            }

            // Division by zero
            {
                Func<object> result = () => new Money(10.2m, Money.CurrencyEnum.GBP) / 0;
                Assert.Throws<DivideByZeroException>(result);
            }

        }
    }
}
