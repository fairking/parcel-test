﻿using ParcelTest.Entities;
using ParcelTest.Helpers;
using ParcelTest.Types;
using System.IO;
using System.Linq;
using Xunit;

namespace ParcelTest.Tests.DomainTests
{
    public class ParcelCsvParserTests
    {
        [Fact]
        public void can_parse_csv()
        {
            var csvPath = Path.Combine(System.AppContext.BaseDirectory, "Files");

            foreach (var csvFile in Directory.GetFiles(csvPath, "*.csv"))
            {
                var batch = CsvParserHelper.Parse(csvFile);
                
                // Serialize
                var xmlFile = Path.Combine(csvPath, Path.GetFileNameWithoutExtension(csvFile) + ".xml");
                XmlSerializerHelper.Serialize(batch, xmlFile);

                // Deserialize
                var batchCopy = XmlSerializerHelper.Deserialize<OrderBatch>(xmlFile);

                // Check with original batch and it's orders
                Assert.Equal(batch, batchCopy);

                // Check Consignments
                Assert.Equal(
                    batch.Orders.SelectMany(x => x.Consignments).OrderBy(x => x.GetHashCode()),
                    batchCopy.Orders.SelectMany(x => x.Consignments).OrderBy(x => x.GetHashCode())
                );

                // Check Parcels
                Assert.Equal(
                    batch.Orders.SelectMany(x => x.Consignments).SelectMany(x => x.Parcels).OrderBy(x => x.GetHashCode()),
                    batchCopy.Orders.SelectMany(x => x.Consignments).SelectMany(x => x.Parcels).OrderBy(x => x.GetHashCode())
                );

                // Check ParcelItems
                Assert.Equal(
                    batch.Orders.SelectMany(x => x.Consignments).SelectMany(x => x.Parcels)
                        .SelectMany(x => x.Items).OrderBy(x => x.GetHashCode()),
                    batchCopy.Orders.SelectMany(x => x.Consignments).SelectMany(x => x.Parcels)
                        .SelectMany(x => x.Items).OrderBy(x => x.GetHashCode())
                );

                // Check TotalWeight
                Assert.Equal(4.604m, batchCopy.TotalWeight);

                // Check TotalValue
                Assert.Collection(batchCopy.TotalValue,
                    v => Assert.Equal(new Money(92.5m, Money.CurrencyEnum.GBP), v),
                    v => Assert.Equal(new Money(80m, Money.CurrencyEnum.EUR), v)
                    );

                File.Delete(csvFile);
            }
        }
    }
}
